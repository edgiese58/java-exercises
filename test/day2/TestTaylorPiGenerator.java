package day2;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import day2.TaylorPiGenerator;

public class TestTaylorPiGenerator {
	TaylorPiGenerator ex;

	@Before
	public void setUp() throws Exception {
		ex = new TaylorPiGenerator();
	}

	@Test
	public void testTaylorPi1() {
		double pi = ex.taylorPi(1);
		double close = 4.0;
		double epsilon = 1.0e-14;
		assertTrue("Value " + pi + " does not equal " + close, Math.abs(pi - close) < epsilon);
	}

	@Test
	public void testTaylorPi2() {
		double pi = ex.taylorPi(2);
		double close = 2.66666666667;
		double epsilon = 1.0e-07;
		assertTrue("Value " + pi + " does not equal " + close, Math.abs(pi - close) < epsilon);
	}

	@Test
	public void testTaylorPi3() {
		double pi = ex.taylorPi(3);
		double close = 3.466666666667;
		double epsilon = 1.0e-7;
		assertTrue("Value " + pi + " does not equal " + close, Math.abs(pi - close) < epsilon);
	}

	@Test
	public void testTaylorPi4() {
		double pi = ex.taylorPi(4);
		double close = 2.8952380952380956;
		double epsilon = 1.0e-14;
		assertTrue("Value " + pi + " does not equal " + close, Math.abs(pi - close) < epsilon);
	}

	@Test
	public void testTaylorPi5() {
		double pi = ex.taylorPi(5);
		double close = 3.3396825396825403;
		double epsilon = 1.0e-14;
		assertTrue("Value " + pi + " does not equal " + close, Math.abs(pi - close) < epsilon);
	}

	@Test
	public void testTaylorPi7() {
		double pi = ex.taylorPi(7);
		double close = 3.2837384837384844;
		double epsilon = 1.0e-14;
		assertTrue("Value " + pi + " does not equal " + close, Math.abs(pi - close) < epsilon);
	}

	@Test
	public void testTaylorPi8() {
		double pi = ex.taylorPi(8);
		double close = 3.017071817071818;
		double epsilon = 1.0e-14;
		assertTrue("Value " + pi + " does not equal " + close, Math.abs(pi - close) < epsilon);
	}
	
	@Test
	public void testTaylorPi9() {
		double pi = ex.taylorPi(9);
		double close = 3.2523659347188767;
		double epsilon = 1.0e-14;
		assertTrue("Value " + pi + " does not equal " + close, Math.abs(pi - close) < epsilon);
	}

	@Test
	public void testTaylorPi10000() {
		double pi = ex.taylorPi(10000);
		double close = 3.1414926535900345;
		double epsilon = 1.0e-14;
		assertTrue("Value " + pi + " does not equal " + close, Math.abs(pi - close) < epsilon);
	}
}
