package day1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestNameFormatter {
	static NameFormatter ex;

	@Before
	public void setUp() throws Exception {
		ex = new NameFormatter();
	}

	@Test
	public void testFullNameJohnDoe() {
		assertEquals("John Doe", ex.fullName("John", null, "Doe"));
	}

	@Test
	public void testFullNameJohnQuincyPublic() {
		assertEquals("John Q. Public", ex.fullName("John", "Quincy", "Public"));
	}

	@Test
	public void testFullNameRaoulRamirezJunior() {
		assertEquals("Raoul Ramirez, Jr.", ex.fullName("Raoul", "", "Ramirez", true));
	}
}
