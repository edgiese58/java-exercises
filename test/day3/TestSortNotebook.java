package day3;

import static org.junit.Assert.*;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import common.Appointment;
import common.NotebookEntry;
import common.Reminder;
import common.ToDo;

public class TestSortNotebook {
	SortNotebook ex;
	Reminder newYears = new Reminder(LocalDate.parse("2010-01-01"), "New Year's Day");
	Reminder jeffBDay = new Reminder(LocalDate.parse("2010-01-01"), "Birthday: Jeff Smith");
	ToDo laundry =      new ToDo(LocalDate.parse("2010-01-01"), "Sort Laundry", Duration.ofMinutes(60L));
	ToDo partyCleanup = new ToDo(LocalDate.parse("2010-01-01"), "Cleanup After Party", Duration.ofMinutes(60L));
	Appointment zoeLunch = new Appointment(LocalDate.parse("2010-01-01"), "Lunch with Zoe", LocalTime.parse("12:00:00"), Duration.ofMinutes(60L));
	Appointment finPlan = new Appointment(LocalDate.parse("2010-01-02"), "Financial Planner", LocalTime.parse("09:00:00"), Duration.ofMinutes(90L));
	ToDo yearEndReport = new ToDo(LocalDate.parse("2010-01-02"), "Finalize Year End Report", Duration.ofMinutes(260L));
	List<NotebookEntry> allEntries = Arrays.asList(newYears, jeffBDay, laundry, partyCleanup, zoeLunch, finPlan, yearEndReport);

	LocalDate jan1 = LocalDate.parse("2010-01-01");
	LocalDate jan2 = LocalDate.parse("2010-01-02");

	@Before
	public void setUp() throws Exception {
		ex = new SortNotebook();
	}

	@Test
	public void testSortNewYearsDay() {
		List<NotebookEntry> sorted = ex.sortNoteBook(allEntries, jan1);
		List<NotebookEntry> shouldBe = Arrays.asList(jeffBDay, newYears, zoeLunch, partyCleanup, laundry);
		assertEquals(shouldBe, sorted);
	}

}
