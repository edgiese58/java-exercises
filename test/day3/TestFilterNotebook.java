package day3;

import static org.junit.Assert.*;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import common.Appointment;
import common.NotebookEntry;
import common.Reminder;
import common.ToDo;

public class TestFilterNotebook {
	FilterNotebook ex;
	Reminder newYears = new Reminder(LocalDate.parse("2010-01-01"), "New Year's Day");
	ToDo laundry =      new ToDo(LocalDate.parse("2010-01-01"), "Sort Laundry", Duration.ofMinutes(60L));
	ToDo partyCleanup = new ToDo(LocalDate.parse("2010-01-01"), "Cleanup After Party", Duration.ofMinutes(60L));
	Appointment zoeLunch = new Appointment(LocalDate.parse("2010-01-01"), "Lunch with Zoe", LocalTime.parse("12:00:00"), Duration.ofMinutes(60L));
	Appointment finPlan = new Appointment(LocalDate.parse("2010-01-02"), "Financial Planner", LocalTime.parse("09:00:00"), Duration.ofMinutes(90L));
	ToDo yearEndReport = new ToDo(LocalDate.parse("2010-01-02"), "Finalize Year End Report", Duration.ofMinutes(260L));
	List<NotebookEntry> allEntries = Arrays.asList(newYears, laundry, partyCleanup, zoeLunch, finPlan, yearEndReport);

	LocalDate jan1 = LocalDate.parse("2010-01-01");
	LocalDate jan2 = LocalDate.parse("2010-01-02");

	@Before
	public void setUp() throws Exception {
		ex = new FilterNotebook();
	}

	@Test
	public void testGetTestToDosAllOneDay() {
		@SuppressWarnings("unchecked")
		List<NotebookEntry> filtered = (List<NotebookEntry>)(Object)ex.getToDos(allEntries, jan1);
		Set<NotebookEntry> shouldBe = new HashSet<>(Arrays.asList(laundry, partyCleanup));
		assertEquals(2, filtered.size());
		assertEquals(shouldBe, new HashSet<NotebookEntry>(filtered));
	}

	@Test
	public void testGetTestToDosAllSingleJan1() {
		@SuppressWarnings("unchecked")
		List<NotebookEntry> filtered = (List<NotebookEntry>)(Object)ex.getToDos(allEntries, jan1, 1);
		Set<NotebookEntry> shouldBe = new HashSet<>(Arrays.asList(laundry, partyCleanup));
		assertEquals(1, filtered.size());
		assertTrue("Filtered todo should be in set", shouldBe.contains(filtered.get(0)));
	}

}
