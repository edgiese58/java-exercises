package day4;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import common.Appointment;
import common.NotebookEntry;
import common.Reminder;
import common.ToDo;

public class TestMergeNotebooks {
	MergeNotebooks ex;
	Reminder newYears = new Reminder(LocalDate.parse("2010-01-01"), "New Year's Day");
	Reminder jeffBDay = new Reminder(LocalDate.parse("2010-01-01"), "Birthday: Jeff Smith");
	ToDo laundry =      new ToDo(LocalDate.parse("2010-01-01"), "Sort Laundry", Duration.ofMinutes(60L));
	ToDo partyCleanup = new ToDo(LocalDate.parse("2010-01-01"), "Cleanup After Party", Duration.ofMinutes(60L));
	Appointment zoeLunch = new Appointment(LocalDate.parse("2010-01-01"), "Lunch with Zoe", LocalTime.parse("12:00:00"), Duration.ofMinutes(60L));
	Appointment finPlan = new Appointment(LocalDate.parse("2010-01-02"), "Financial Planner", LocalTime.parse("09:00:00"), Duration.ofMinutes(90L));
	Appointment sprintPlan = new Appointment(LocalDate.parse("2010-01-02"), "Dev Team Sprint Planning", LocalTime.parse("10:30:00"), Duration.ofMinutes(90L));
	ToDo yearEndReport = new ToDo(LocalDate.parse("2010-01-02"), "Finalize Year End Report", Duration.ofMinutes(260L));
	List<NotebookEntry> allEntries = Arrays.asList(newYears, jeffBDay, laundry, partyCleanup, zoeLunch, finPlan, sprintPlan, yearEndReport);

	LocalDate jan1 = LocalDate.parse("2010-01-01");
	LocalDate jan2 = LocalDate.parse("2010-01-02");
	Appointment clash = new Appointment(LocalDate.parse("2010-01-02"), "Budget Review", LocalTime.parse("09:30:00"), Duration.ofMinutes(60L));


	@Before
	public void setUp() throws Exception {
		ex = new MergeNotebooks();
	}

	@Test
	public void testMergeTwoSingles() {
		List<NotebookEntry> single1 = Arrays.asList(newYears);
		List<NotebookEntry> single2 = Arrays.asList(jeffBDay);
		List<NotebookEntry> merged = ex.mergeNoteBooks(single1, single2);
		assertEquals(2, merged.size());
		assertEquals(new HashSet<NotebookEntry>(merged), new HashSet<NotebookEntry>(Arrays.asList(newYears, jeffBDay)));
	}

	@Test
	public void testMergeTwoSinglesOneDup() {
		List<NotebookEntry> doubled = Arrays.asList(newYears, jeffBDay);
		List<NotebookEntry> single = Arrays.asList(jeffBDay);
		List<NotebookEntry> merged = ex.mergeNoteBooks(single, doubled);
		assertEquals(2, merged.size());
		assertEquals(new HashSet<NotebookEntry>(merged), new HashSet<NotebookEntry>(Arrays.asList(newYears, jeffBDay)));
	}

	@Test
	public void testMergeTwoTouchingAppts() {
		List<NotebookEntry> single1 = Arrays.asList(finPlan);
		List<NotebookEntry> single2 = Arrays.asList(sprintPlan);
		List<NotebookEntry> merged = ex.mergeNoteBooks(single1, single2);
		assertEquals(2, merged.size());
		assertEquals(new HashSet<NotebookEntry>(merged), new HashSet<NotebookEntry>(Arrays.asList(finPlan, sprintPlan)));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testMergeClashAppts() {
		List<NotebookEntry> single1 = Arrays.asList(finPlan);
		List<NotebookEntry> single2 = Arrays.asList(clash);
		List<NotebookEntry> merged = ex.mergeNoteBooks(single1, single2);
	}

	@Test
	public void testMergeIdentical() {
		assertEquals(allEntries, ex.mergeNoteBooks(allEntries, allEntries));
	}
}
