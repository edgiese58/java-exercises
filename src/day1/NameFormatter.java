package day1;

public class NameFormatter {
	/**
	 * Given a first, middle, and last name, returns a full name, with optional "Jr.".  The middle
	 * name will be reduced to an initial.
	 * 
	 * @param first the first name
	 * @param middle the middle name. can be an empty string or null if no middle name exists
	 * @param last the last name
	 * @param isJunior if true, it's a "Junior"
	 * @return the full name
	 */
	public String fullName(String first, String middle, String last, boolean isJunior) {
		return "";
	}
	// default isJunior to false
	public String fullName(String first, String middle, String Last) {
		return this.fullName(first, middle, Last, false);
	}
}
