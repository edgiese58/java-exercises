package day2;

public class TaylorPiGenerator {
	/**
	 * Generates Pi to varying degrees of accuracy
	 * Uses the formula 4 * (1 - 1/3 + 1/5 - 1/7 + 1/9 - ....)
	 * @param nTerms  The number of terms, 1 term is just the "1" term
	 * @return double the calculated pi
	 */
	public double taylorPi(int nTerms) {
		return 0.0;
	}
}
