package common;

import java.time.Duration;
import java.time.LocalDate;

public class ToDo extends TimeTaker {

	public ToDo(LocalDate ld, String desc, Duration dur) {
		super(ld, desc, dur);
	}
}
