package common;

import java.time.Duration;
import java.time.LocalDate;

public class TimeTaker extends NotebookEntry {
	private final Duration duration;

	public TimeTaker(LocalDate ld, String desc, Duration dur) {
		super(ld, desc);
		this.duration = dur;
	}

	public Duration getDuration() {
		return duration;
	}
}
