package common;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class NotebookEntry {
	private final LocalDate date;
	private final String description;
	
	public NotebookEntry(LocalDate ld, String desc) {
		this.description = desc;
		this.date = ld;
	}

	public LocalDate getDate() {
		return date;
	}
	
	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return "Notebook Entry for " + date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)) + ": " + description;
	}
	
}
