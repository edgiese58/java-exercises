package common;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;

public class Appointment extends TimeTaker {
	private final LocalTime time;

	public Appointment(LocalDate ld, String desc, LocalTime t, Duration dur) {
		super(ld, desc, dur);
		this.time = t;
	}

	public LocalTime getTime() {
		return time;
	}
}
