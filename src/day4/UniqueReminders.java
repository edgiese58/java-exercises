package day4;

import java.util.ArrayList;
import java.util.List;

import common.NotebookEntry;
import common.Reminder;

public class UniqueReminders {
	/**
	 * From a list of notebook items, return unique Reminders
	 * Reminders should be considered the same in a case insensitive way
	 * @param nbe a list containing Appointments, ToDos, and Reminders
	 * @return the list of Reminders.
	 */
	List<Reminder> uniqueReminders(List<NotebookEntry> nbe) {
		return new ArrayList<Reminder>();
	}

}
