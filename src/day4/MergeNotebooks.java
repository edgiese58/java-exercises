package day4;

import java.util.ArrayList;
import java.util.List;

import common.NotebookEntry;

public class MergeNotebooks {
	/**
	 * Merges two notebooks.  Merges duplicate entries
	 * NOTE:  This method is intended for notebooks that have been checked to make certain that
	 * no Appointments overlap.
	 * @param nbe
	 * @param nbe2
	 * @return merged notebook
	 * @throws IllegalArgumentException if there is overlap of appointment times
	 */
	List<NotebookEntry> mergeNoteBooks(List<NotebookEntry> nbe, List<NotebookEntry>nbe2) throws IllegalArgumentException {
		return new ArrayList<NotebookEntry>();
	}

}
