package day3;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import common.NotebookEntry;
import common.ToDo;

public class FilterNotebook {
	public static final int ALL_TODOS = -1;

	/**
	 * From a list of notebook items, return n number of ToDos
	 * @param nbe a list containing Appointments, ToDos, and Reminders
	 * @param ld LocalDate the date to get todos.  If null, get them all
	 * @param nTodos the number of ToDos.  if it is ALL_TODOS, then return all of them
	 * @return the list of ToDos.
	 */
	List<ToDo> getToDos(List<NotebookEntry> nbe, LocalDate ld, int nTodos) {
		return new ArrayList<ToDo>();
	}

	List<ToDo> getToDos(List<NotebookEntry> nbe, LocalDate ld) {
		return this.getToDos(nbe, ld, ALL_TODOS);
	}
}
