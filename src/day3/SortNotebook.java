package day3;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import common.NotebookEntry;

public class SortNotebook {
	/**
	 * Sort a list of notebook entries for the purposes of displaying them.
	 * Required sort order:
	 * 1.  All Reminders, in alphabetical order by description
	 * 2.  All Appointments, in order by time
	 * 3.  All ToDos, in alphabetical order by description
	 * @param nbe a list containing Appointments, ToDos, and Reminders
	 * @param ld the date to get sorted list for
	 * @return the sorted list
	 */
	List<NotebookEntry> sortNoteBook(List<NotebookEntry> nbe, LocalDate ld) {
		return new ArrayList<NotebookEntry>();
	}
}
